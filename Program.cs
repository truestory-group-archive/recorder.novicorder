﻿using Recorder.Controllers.Keyboard;
using Recorder.Core;
using Recorder.Core.Interfaces;
using SimpleInjector;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Recorder.NoviCorder
{
    class Program
    {
        static void Main(string[] args)
        {
            // 2. Configure the container (register)
            var bootstrapper = new Recorder.Core.Bootstrapper();
            var container = bootstrapper.Initialize();
            
            container.Register<IAudioDiscovery, Recorder.CSCore.AudioDiscovery>(Lifestyle.Singleton);
            container.Register<IVideoDiscovery, Recorder.AForge.Net.VideoDiscovery>(Lifestyle.Singleton);

            container.Register<IAudioRecorderFactory, Recorder.CSCore.AudioRecorderFactory>(Lifestyle.Singleton);
            container.Register<IVideoRecorderFactory, Recorder.AForge.Net.VideoRecorderFactory>(Lifestyle.Singleton);

            container.Register<ISavePathService>(() => {
                var savePath = System.Configuration.ConfigurationManager.AppSettings["Recorder.SavePath"];
                return new DefaultSavePathService(savePath);
            }, Lifestyle.Singleton);

            container.Register<IRecordingController, Recorder.Controllers.Keyboard.KeyboardRecordingController> (Lifestyle.Singleton);
            container.RegisterCollection<IStatusController>(new[] {
                typeof(Recorder.Core.Models.ConsoleStatusController)
            });

            container.Register<IKeyboardConfiguration>(() => {
                var startKey = System.Configuration.ConfigurationManager.AppSettings["Recorder.Controller.Keyboard.Start"].ToCharArray();
                var stopKey = System.Configuration.ConfigurationManager.AppSettings["Recorder.Controller.Keyboard.Stop"].ToCharArray();

                return new SimpleKeyboardConfiguration(startKey[0], stopKey[0]);
            }, Lifestyle.Singleton);
            
            var registration = Lifestyle.Singleton.CreateRegistration<Implementations.DeviceFilter>(container);
            container.AddRegistration(typeof(IVideoDeviceFilter), registration);
            container.AddRegistration(typeof(IAudioDeviceFilter), registration);
            
            if(System.Configuration.ConfigurationManager.AppSettings["Recorder.Status.UseBlinkStick"] == "true")
                container.RegisterCollection<IStatusController>(new[]
                {
                    typeof(Recorder.Status.BlinkStick.BlinkStickStatus)
                });

            // 3. Optionally verify the container's configuration.
            container.Verify();

            bootstrapper.Exiting += new Bootstrapper.ExitEventHandler(OnExit);

            bootstrapper.Run();
            
            // 4. Wait
            var doWork = Task.Run(() =>
            {
                while (true) {
                    //Console.WriteLine(i);
                    Thread.Sleep(1000);
                }
            });
            Application.Run();
        }

        private static void OnExit(object sender, EventArgs e)
        {
            // Shutdown computer
            var psi = new ProcessStartInfo("shutdown", "/s /t 0");
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            Process.Start(psi);

            Application.Exit();
        }
    }
}
