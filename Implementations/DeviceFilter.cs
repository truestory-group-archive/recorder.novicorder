﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.NoviCorder.Implementations
{
    public class DeviceFilter : IVideoDeviceFilter, IAudioDeviceFilter
    {
        private ILogger _logger;

        public DeviceFilter(ILogger logger)
        {
            _logger = logger;
        }

        public IEnumerable<IAudioDevice> Filter(IEnumerable<IAudioDevice> devices)
        {
            _logger.Log("===== Started Audio Device Filtering =====");
            var filtered = new List<IAudioDevice>();
            foreach (var d in devices)
            {
                var name = d.Name.ToLower();
                _logger.Log(string.Format("[AutoAudio] Available Device: '{0}'", name));

                if (name.Contains("usb pnp sound device") || name.Contains("(2- usb") || name.Contains("c-media"))
                {
                    _logger.Log(string.Format("-> Selected", d.Name));
                    filtered.Add(d);
                }
            }

            _logger.Log("===== Finished Audio Device Filtering =====\n");
            return filtered;
        }

        public IEnumerable<IVideoDevice> Filter(IEnumerable<IVideoDevice> devices)
        {
            _logger.Log("===== Started Video Device Filtering =====");
            var filtered = new List<IVideoDevice>();
            foreach (var d in devices)
            {
                var name = d.Name.ToLower();
                _logger.Log(string.Format("[AutoVideo] Available Device: '{0}'", name));
                if (name.Contains("logitech"))
                {
                    _logger.Log(string.Format("-> Selected", d.Name));
                    filtered.Add(d);
                }
            }

            _logger.Log("===== Finished Video Device Filtering =====\n");
            return filtered;
        }
    }
}
